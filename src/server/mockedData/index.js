import login from './login';
import registerMovie from './registerMovie';
import agencyMovies from './agencyMovies';

export default {
  login,
  registerMovie,
  agencyMovies,
};
