export default {
  "conection": true,
  "error": null,
  "movie": {
      "info": {
          "query": true,
          "insert_id": 0,
          "affected": 1,
          "warning": 1,
          "debuggin": "",
          "trasaction": "INSERT INTO movie (id,title,original_title,vote_average,poster_path,backdrop_path,overview,release_date,timestamp,views) VALUES (918,\"Cita a ciegas\",\"Blind Date\",5.8,\"/aK8GWfcGS9zb78ksDBZBBNp2Y1X.jpg\",\"/wqCaOtRz1Bs6U8C0umTWLvxTTgo.jpg\",\"Walter Davis es un ejecutivo adicto al trabajo que dedica muy poco tiempo a su vida privada. Un día necesita una acompañante para ir a una cena de negocios con un cliente japonés, y su hermano le propone ir acompañado de Nadia, una chica nueva en la ciudad.\",\"1987-03-27\",1547249273,0)"
      }
  },
  "r_movie_genre": {
      "info": {
          "query": true,
          "insert_id": 1005,
          "affected": 1,
          "warning": 1,
          "debuggin": "",
          "trasaction": "INSERT INTO r_movie_genre (id,id_movie,id_genre) VALUES (\"\",918,10749)"
      }
  },
  "casting": {
      "info": {
          "query": true,
          "insert_id": 0,
          "affected": 1,
          "warning": 0,
          "debuggin": "",
          "trasaction": "INSERT INTO casting (id,cast_id,personage,credit_id,name,profile_path) VALUES (29446,28,\"\",\"569503ce9251414b64002113\",\"Armin Shimerman\",\"/9THIxwTKPI5MrPPmLmwzU1CpQkl.jpg\")"
      }
  },
  "r_movie_cast": {
      "info": {
          "query": true,
          "insert_id": 9220,
          "affected": 1,
          "warning": 1,
          "debuggin": "",
          "trasaction": "INSERT INTO r_movie_cast (id,id_movie,id_casting) VALUES (\"\",918,29446)"
      }
  },
  "crew": {
      "info": {
          "query": true,
          "insert_id": 0,
          "affected": 1,
          "warning": 1,
          "debuggin": "",
          "trasaction": "INSERT INTO crew (credit_id,department,gender,id,job,name,profile_path) VALUES (\"58b987a4c3a368664d00b750\",\"Production\",2,11405,\"Co-Executive Producer\",\"David Permut\",\"/7pAjvPIcB8a6aGefNJqMMvqKOKU.jpg\")"
      }
  },
  "r_movie_crew": {
      "info": {
          "query": true,
          "insert_id": 14502,
          "affected": 1,
          "warning": 1,
          "debuggin": "",
          "trasaction": "INSERT INTO r_movie_crew (id,id_movie,id_crew) VALUES (\"\",918,11405)"
      }
  }
};