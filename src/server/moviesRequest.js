import axios from 'axios';

const baseUrl = 'https://api.themoviedb.org/3/search/multi?api_key=7e05c7b5171afd3cd423d7163acf39a4&language=es&query=';

const searchMovie = (query) => axios.get(`${baseUrl}${query}`);

export { searchMovie };


