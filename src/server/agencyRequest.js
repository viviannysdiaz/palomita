import axios from 'axios';

// REMOVE THIS IMPORT
import mockedData from './mockedData';

const baseUrl = 'http://api.iptv.agenciaym.com/v1';

const agencyRequest = {
  login: (email, password) => axios.post(`${baseUrl}/iptv/login`, { email, password }),
  registerMovie: (movieId) => axios.post(`${baseUrl}/movie/${movieId}`),
  getMovies: (page, query) => axios.get(`${baseUrl}/movie/?page=${page}&query=${query}`),
  registerVideos: (movieId, data) => axios.put(`${baseUrl}/movie/${movieId}/video`, data),
};

// MOCKED ENPOINTS
const agencyRequestMocked = {
  login: (email, password) => new Promise((resolve, reject) => {
    setTimeout(() => {
      if (email === 'test@test.com' && password === '123456') {
        return resolve({ data: mockedData.login });
      }
      reject();
    }, 250);
  }),
  registerMovie: (movieId) => new Promise((resolve) => {
    setTimeout(() => {
      return resolve({ data: mockedData.registerMovie });
    }, 250);
  }),
  getMovies: (page) => new Promise((resolve) => {
    setTimeout(() => {
      return resolve({ data: mockedData.agencyMovies[page] });
    }, 250);
  }),
  registerVideos: (movieId, data) => new Promise((resolve) => {
    setTimeout(() => {
      return resolve({});
    }, 250);
  }),
};

// CHANGE THIS TO THE REAL OBJECT (agencyRequest)
export default agencyRequestMocked;
