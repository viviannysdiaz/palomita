import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import { Login, Board } from './components';

const AppRouter = () => (
  <Router>
    <div>
      <Route path="/login" component={Login} />
      <Route path="/board" component={Board} />
    </div>
  </Router>
);

export default AppRouter;
