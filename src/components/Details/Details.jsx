import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import agencyRequest from '../../server/agencyRequest';

class Details extends Component {
  static propTypes = {
    movie: PropTypes.object.isRequired,
  };

  componentWillReceiveProps (nextProps) {
    this.setState({
      videosModal: false,
      videos: [
        {
          id: null,
          title: nextProps.movie.name,
          url: '',
        },
      ],
    });
  }

  state = {
    videosModal: false,
    videos: [
      {
        id: null,
        title: this.props.movie.name,
        url: '',
      },
    ],
  };

  onClickRegister = () => {
    this.setState({
      videosModal: true,
      saving: true,
    }, () => this.saveMovie());
  };

  onChangeInput = (index, key) => (e) => {
    const { videos } = this.state;
    const { value } = e.target;
    let newVideos = [...videos];
    newVideos[index][key] = value;
    this.setState({
      videos: newVideos,
    });
  }
  

  saveMovie = () => {
    const { movie } = this.props;
    agencyRequest.registerMovie(movie.id)
      .then(response => {
        this.setState({
          saving: false,
        });
      });
  } 

  addInput = () => {
    this.setState({
      videos: [
        ...this.state.videos,
        {
          id: null,
          title: this.props.movie.name,
          url: '',
        },
      ],
    });
  }

  onRemoveInput = (index) => {
    const { videos } = this.state;
    let newVideos = videos.filter((_, indexItem) => indexItem !== index);
    this.setState({
      videos: newVideos,
    });
  }

  saveVideos = () => {
    agencyRequest.registerVideos(this.props.movie.id, this.state.videos)
      .then(response => {
        window.$('#detailsModal').modal('hide');
        window.$('#registerSuccess').modal('show');
      });
  }

  renderInputs = () => {
    return this.state.videos.map((video, index) => (
      <div key={index}>
        <p className="font-weight-bold">
          Video #{index + 1}
          {index !== 0 &&
            <i 
              style={{ cursor: 'pointer' }} 
              className="fas fa-times text-danger ml-2" 
              onClick={() => this.onRemoveInput(index)}
            />
          }
        </p>
        <div className="form-group">
          <label htmlFor="email" className="sr-only">Titulo</label>
          <input 
            value={this.state.videos[index].title}
            onChange={this.onChangeInput(index, 'title')}
            type="text" 
            name="text" 
            id="title" 
            className="form-control"
            placeholder="Titulo"
          />
        </div>
        <div className="form-group">
          <label htmlFor="email" className="sr-only">Url</label>
          <input 
            value={this.state.videos[index].url}
            onChange={this.onChangeInput(index, 'url')}
            type="text" 
            name="text" 
            id="url" 
            className="form-control" 
            placeholder="Url"
          />
        </div>
      </div>
    ));
  }

  renderSuccess = () => {
    return (
      <div className="modal fade" id="registerSuccess" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div className="modal-dialog" role="document">
              <div className="modal-content">
              <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLongTitle">Registro Exitoso</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div className="modal-body">
                Los datos de los videos fueron guardados exitosamente...
              </div>
              <div className="modal-footer">
                  <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              </div>
              </div>
          </div>
      </div>
    );
  }

  detailsContent = () => {
    const { movie } = this.props;

    return (
      <div className="modal fade" id="detailsModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
          <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">{movie.original_name || movie.name}</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div className="modal-body">
              <div className="m-2">
                <span className="font-weight-bold">Nombre Original: </span>{movie.original_name}
              </div>
              <div className="m-2">
                <span className="font-weight-bold">Primera emisión: </span>{movie.first_air_date}
              </div>
              <div className="m-2">
                <span className="font-weight-bold">Medio de difusión: </span>{movie.media_type}
              </div>
              <div className="m-2">
                <span className="font-weight-bold">Popularidad: </span>{movie.popularity}
              </div>
              <div className="m-2">
                <span className="font-weight-bold">Promedio de votos: </span>{movie.vote_average}
              </div>
              <div className="m-2">
                <span className="font-weight-bold">Total de Votos: </span>{movie.vote_count}
              </div>
              <div className="m-2">
                <span className="font-weight-bold">Sinopsis:</span>
                <p className="text-justify">{movie.overview}</p>
              </div>
          </div>
          <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="button" className="btn btn-primary" onClick={this.onClickRegister}>Guardar Pelicula</button>
          </div>
          </div>
        </div>
      </div>
    );
  }

  videosContent = () => {
    const { movie } = this.props;

    return (
      <div className="modal fade" id="detailsModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLongTitle">Crear Videos de {movie.original_name || movie.name}</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div className="modal-body">
                {this.state.saving ?
                  <div className=" text-center mt-5">
                    <i className="fas fa-spinner fa-spin fa-3x"></i>
                  </div>
                :
                  <div>
                    {this.renderInputs()}
                    <div>
                        <button type="button" className="btn btn-primary btn-block" onClick={this.addInput}> Añadir Otro Video </button>
                    </div>
                  </div>
                }
            </div>
            
            <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" className="btn btn-primary" onClick={this.saveVideos}>Guardar Videos</button>
            </div>
          </div>
        </div>
      </div>
    )
  }

  render() {
    return (
      <Fragment>
        {!this.state.videosModal 
          ? this.detailsContent()
          : this.videosContent()
        }
        {this.renderSuccess()}
      </Fragment>
    );
  }
}

export default Details;
