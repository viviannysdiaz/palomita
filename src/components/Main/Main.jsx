import React, { Component, Fragment } from 'react';

const ProtectedRoutes = (WrappedComponent) => {
  return class extends Component {

    state = {
      isLogged: false,
      loading: true,
    }

    componentDidMount() {
      const user = JSON.parse(localStorage.getItem('user'));
      if (!user) {
        window.location.href = "/login";
      }
    }

    onLogout = () => {
      localStorage.removeItem('user');
      window.location.href = "/login";
    }

    render() {
      // Wraps the input component in a container, without mutating it. Good!
      return (
        <Fragment>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
              <span className="navbar-brand">Palomita</span>
              <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                <li className="nav-item active">
                  <span className="nav-link">Panel <span className="sr-only">(current)</span></span>
                </li>
              </ul>
              <form className="form-inline my-2 my-lg-0">
                <button onClick={this.onLogout} className="btn btn-primary my-2 my-sm-0" type="button">Cerrar Sesion</button>
              </form>
            </div>
          </nav>
          <WrappedComponent {...this.props} />
        </Fragment>
      );
    }
  }
}

export default ProtectedRoutes;
