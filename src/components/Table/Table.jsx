import React, { Component } from 'react';
import agencyRequest from '../../server/agencyRequest';

class Table extends Component {
  state = {
    page: 1,
    movies: [],
  };

  componentDidMount() {
    agencyRequest.getMovies(this.state.page)
      .then(response => {
        this.setState({
          movies: response.data,
        });
      });
  }

  getMovies = (page) => {
    if (page < 1) return;
    agencyRequest.getMovies(page)
    .then(response => {
      this.setState({
        page,
        movies: response.data,
      });
    });
  }

  renderRows = () => {
    if(!this.state.movies || this.state.movies.length === 0) return;
    return this.state.movies.map(movie => (
      <tr key={movie.id}>
        <th scope="row">{movie.id}</th>
        <td>{movie.title}</td>
        <td>{movie.original_title}</td>
        <td>{movie.release_date}</td>
      </tr>
    ));
  }

  render() {
    return (
      <div className="mt-5">
        <table className="table">
          <thead className="thead-light">
            <tr>
            <th scope="col">Id</th>
            <th scope="col">Titulo</th>
            <th scope="col">Titulo Original</th>
            <th scope="col">Estreno</th>
            </tr>
          </thead>
          <tbody>
            {this.renderRows()}
          </tbody>
        </table>
        <nav aria-label="Page navigation example">
          <ul className="pagination">
            <li className="page-item"><button onClick={() => this.getMovies(this.state.page - 1)} className="page-link" href="#">Previous</button></li>
            <li className="page-item"><button onClick={() => this.getMovies(this.state.page + 1)} className="page-link" href="#">Next</button></li>
          </ul>
        </nav>
      </div>
    );
  }
}

export default Table;