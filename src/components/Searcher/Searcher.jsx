import React, { Component } from 'react';
import Details from '../Details';
import { searchMovie } from '../../server/moviesRequest';

const imgBaseUrl = 'http://image.tmdb.org/t/p/w185//';

class Searcher extends Component {
  state = {
    query: '',
    searching: false,
    results: [],
    selectedMovie: {},
  };

  onInputChange = (e) => {
    const { value } = e.target;
    this.setState({
      query: value,
      searching: value !== '',
      results: [],
    }, () => value !== '' ? this.search() : null);
  }

  onClickItem = (item) => {
    window.$('#detailsModal').modal('show');
    this.setState({
      selectedMovie: item,
    });
  }

  search = () => {
    searchMovie(this.state.query)
      .then(response => {
        this.setState({
          searching: false,
          results: response.data.results
        })
      })
      .catch(() => this.setState({ 
        results: [],
        searching: false,
      }));
  }

  renderCards = () => {
    if (this.state.results.length === 0) return;
    return this.state.results.map((movie) => (
      <div className="card mb-3" style={{ width: '20rem' }}>
        <img src={`${imgBaseUrl}${movie.poster_path}`} className="card-img-top" alt={movie.original_name || 'Sin nombre original'}/>
        <div className="card-body">
          <h5 className="card-title mb-3">{movie.original_name || 'Sin nombre original'}</h5>
          <h6 className="card-subtitle text-muted">
            {movie.name || 'Sin nombre'}
          </h6>
          <h6 className="font-italic mb-3 text-muted">
            {movie.first_air_date || 'Sin fecha de estreno'}
          </h6>
          <p className="card-text" style={{ minHeight: '10rem' }}>
            {movie.overview && movie.overview.length > 0 ? movie.overview.substring(0, 200) : 'Sin Descripción'}...
          </p>
          <button onClick={() => this.onClickItem(movie)} type="button" className="btn btn-link">Ver detalles</button>
        </div>
      </div>
    ));
  }

  render() {
    return (
      <div>
        <div className="input-group suggestionsContainer mt-5">
          <input 
            type="text" 
            className="form-control" 
            placeholder="Buscador" 
            aria-label="Buscador" 
            aria-describedby="basic-addon2"
            onChange={this.onInputChange}
            value={this.state.query}
          />
        </div>
        <div className="mt-5">
          {this.state.searching ?
            <div className=" text-center mt-5">
              <i className="fas fa-spinner fa-spin fa-3x"></i>
            </div>
          :
            <div className="cardsContainer mt-5">
                {this.renderCards()}
            </div>
          }
        </div>
        <Details movie={this.state.selectedMovie}/>
      </div>
    );
  }
}

export default Searcher;