import React, { Component } from 'react';
import Searcher from '../Searcher';
import ProtectedRoutes from '../Main';
import Table from '../Table';
import './styles.css';

class Board extends Component {
  render() {
    return (
      <div className="container mt-5 maincontainer">
        <ul className="nav nav-tabs" id="myTab" role="tablist">
          <li className="nav-item">
            <a className="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Buscador</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Peliculas Guardadas</a>
          </li>
        </ul>
        <div className="tab-content" id="myTabContent">
          <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <Searcher/>
          </div>
          <div className="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <Table/>
          </div>
        </div>
      </div>
    );
  }
}

export default ProtectedRoutes(Board);
