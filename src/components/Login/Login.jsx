import React, { Component } from 'react';
import agencyRequest from '../../server/agencyRequest';
import './styles.css';
class Login extends Component {
  state = {
    email: '',
    password: '',
    showPassword: false,
  };

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user) {
      window.location.href = "/board";
    }
  }

  onInputEmail = (e) => {
    const { value } = e.target;
    this.setState({
      email: value,
    });
  }

  onInputPassword = (e) => {
    const { value } = e.target;
    this.setState({
      password: value,
    });
  }

  onClickShowPassword = () => {
    this.setState((state) => ({
      showPassword: !state.showPassword,
    }));
  }

  onLogin = () => {
    const { email, password } = this.state;
    if (email === '' || password === '') {
      return;
    }
    agencyRequest.login(email, password)
      .then(response => {
        localStorage.setItem('user', JSON.stringify(response.data));
        window.location.href = "/board";
      })
      .catch(error => {
        window.$('#errorLogin').modal('show');
      });
  }

  renderError = () => {
    return (
      <div className="modal fade" id="errorLogin" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div className="modal-dialog" role="document">
              <div className="modal-content">
              <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLongTitle">Error!</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div className="modal-body">
                Credenciales Invalidas...
              </div>
              <div className="modal-footer">
                  <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              </div>
              </div>
          </div>
      </div>
    );
  }

  render() {
    return (
      <div className="jumbotron vertical-center">
          <div className="container ">
            <div className="row d-flex justify-content-center">
                <div className="col-xs-12">
                    <div className="form-wrap">
                      <h1>Inicio de Sesion</h1>
                          <form method="post" id="login-form">
                              <div className="form-group">
                                  <label htmlFor="email" className="sr-only">Email</label>
                                  <input onChange={this.onInputEmail} value={this.state.email} type="email" name="email" id="email" className="form-control" placeholder="nombre@ejemplo.com" />
                              </div>
                              <div className="form-group">
                                  <label htmlFor="key" className="sr-only">Contraseña</label>
                                  <input onChange={this.onInputPassword} value={this.state.password} type={!this.state.showPassword ? 'password' : 'text'} name="key" id="key" className="form-control" placeholder="Contraseña" />
                              </div>
                              <div className="checkbox">
                                  <input onChange={this.onClickShowPassword} type="checkbox" className="mr-2"/>
                                  <span className="label"><small>Mostrar contraseña</small></span>
                              </div>
                              <button onClick={this.onLogin}type="button" className="btn btn-primary mt-2">Iniciar Sesion</button>
                          </form>
                    </div>
                </div>
            </div>
          </div>
          {this.renderError()}
      </div>
    );
  }
}

export default Login;
